/* Import pages */
import Signup from './pages/Signup';
import Shop from './pages/Shop';
import ProductDetails from './pages/ProductDetails';
import Login from './pages/Login';
import Home from './pages/Home';
import Checkout from './pages/Checkout';
import Cart from './pages/Cart';

import Router from './routers/Router';

export { Signup, Shop, ProductDetails, Login, Home, Checkout, Cart, Router };
