import LOGO from './images/eco-logo.png';
import USER_ICON from './images/user-icon.png';

export { LOGO, USER_ICON };
