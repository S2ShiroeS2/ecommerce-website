import React from 'react';
import { Helmet } from '../components/compRoot';

const Home = () => {
	return <Helmet title={'Home'}></Helmet>;
};

export default Home;
