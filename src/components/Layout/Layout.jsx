import React from 'react';
import { Header, Footer } from '../compRoot';
import { Router } from '../../root';

function Layout() {
	return (
		<>
			<Header />
			<div>
				<Router />
			</div>
			<Footer />
		</>
	);
}

export default Layout;
