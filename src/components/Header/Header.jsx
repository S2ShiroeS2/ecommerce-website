import React from 'react';
import './Style-header.css';
import { NavLink } from 'react-router-dom';
import { Container, Row } from 'reactstrap';
import { motion } from 'framer-motion';
import { LOGO, USER_ICON } from '../../assets/assetsRoot';

const navLinks = [
	{ path: 'home', title: 'Home' },
	{ path: 'shop', title: 'Shop' },
	{ path: 'cart', title: 'Cart' }
];

const Header = () => {
	return (
		<header className="header">
			<Container>
				<Row>
					<div className="nav__wrapper">
						<div className="logo">
							<img src={LOGO} alt="Logo" />
							<div className="logo__content">
								<h1 className="logo__name">Emy Market</h1>
								<p className="logo__founding">Since 1995</p>
							</div>
						</div>
						<div className="nav__navbar">
							<ul className="nav__menu">
								{navLinks.map((item, index) => (
									<li className="nav__item" key={index}>
										<NavLink
											to={item.path}
											className={(navClass) =>
												navClass.isActive
													? 'nav__active'
													: ''
											}
										>
											{item.title}
										</NavLink>
									</li>
								))}
							</ul>
						</div>
						<div className="nav__icons">
							<span className="fav__icon">
								<i className="ri-heart-line"></i>
								<span className="nav__badge">1</span>
							</span>
							<span className="cart__icon">
								<i className="ri-shopping-bag-line"></i>
								<span className="nav__badge">1</span>
							</span>
							<span className="user__icon">
								<motion.img
									whileTap={{ scale: 1.2 }}
									src={USER_ICON}
									alt="user-icon"
								/>
							</span>
						</div>
						<div className="mobile__menu">
							<span className="menu__icon">
								<i className="ri-menu-line"></i>
							</span>
						</div>
					</div>
				</Row>
			</Container>
		</header>
	);
};

export default Header;
