import { Routes, Route, Navigate } from 'react-router-dom';
import {
	Signup,
	Shop,
	ProductDetails,
	Login,
	Home,
	Checkout,
	Cart
} from '../root';

const Router = () => {
	return (
		<Routes>
			<Route path="/" element={<Navigate to="home" />} />
			<Route path="home" element={<Home />} />
			<Route path="shop" element={<Shop />} />
			<Route path="shop/:id" element={<ProductDetails />} />
			<Route path="cart" element={<Cart />} />
			<Route path="checkout" element={<Checkout />} />
			<Route path="login" element={<Login />} />
			<Route path="signup" element={<Signup />} />
		</Routes>
	);
};

export default Router;
