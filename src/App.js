import './App.css';
import { Layout } from './components/compRoot';

function App() {
	return <Layout />;
}

export default App;
